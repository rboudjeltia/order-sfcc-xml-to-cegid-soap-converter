# SFCC orders in SOAP request for Cegid-Y2 #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Create a SOAP request for CEGID-Y2 based on XML orders from SFCC
* 0.1


### How do I get set up? ###

#### Dependencies ####
```python
from xml.etree import cElementTree as ElementTree
import lxml.etree as etree
from pprint import pprint
import json
import sys
from xml.sax.saxutils import escape
from xml.dom.minidom import Text, Element
```

#### Execution ####
```shell
python main.py my_file.xml SITE_ID xml

# SAMPLE
python main.py LOE_USA_orders.xml LOE_USA xml > LOE_USA_SOAPs.xml
```



It's possible sometimes you got a problem like the following one 

```
Traceback (most recent call last):
  File "order-conv.py", line 364, in <module>
    main(sys.argv)
  File "order-conv.py", line 355, in main
    for i in range(0, len(xmldict['orders']['order'])) :
KeyError: 'orders'
```

In this case, add a <orders> after the root node and </orders> at the end

```xml
<?xml version="1.0" encoding="UTF-8"?>
<orders xmlns="http://www.demandware.com/xml/impex/order/2006-10-31">
    <orders>
    <order order-no="PRD-00048220">
	
	[....]

   </order>
</orders>
</orders>
```


### Who do I talk to? ###

* Reda boudjeltia