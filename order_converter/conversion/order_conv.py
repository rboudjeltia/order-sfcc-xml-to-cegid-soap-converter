from xml.dom import minidom
from xml.sax.saxutils import escape
import json
import os


################## UTILS 


def getNodeText(node):
    nodelist = node.childNodes
    result = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            result.append(node.data)
    return ''.join(result)


def getShippingAddress(node):
    '''Shipping data'''
    info = {}
    shipment = node.getElementsByTagName("shipments")[0].getElementsByTagName("shipment")[0]
    ship = shipment.getElementsByTagName("shipping-address")[0]
    customs = ship.getElementsByTagName("custom-attribute")
    totals = node.getElementsByTagName('shipping-lineitems')[0].getElementsByTagName('shipping-lineitem')[0]
    for node1 in totals.childNodes:
        info[str(node1.nodeName)] = getNodeText(node1)
    for node2 in ship.childNodes:
        info[str(node2.nodeName)] = getNodeText(node2)
    for node2 in customs:
        info[str(node2.attributes.item(0).value)] = getNodeText(node2)

    gift = getNodeText(shipment.getElementsByTagName("gift")[0])
    if(gift == 'true'):
        info['gift'] = gift
        if(len(shipment.getElementsByTagName("gift-message")) > 0):
            info['gift-message'] = getNodeText(shipment.getElementsByTagName("gift-message")[0])
        else:
            info['gift-message'] = ''

    return info


def getBillingAddress(node):
    '''Billing data'''
    info = {}
    bill = node.getElementsByTagName("customer")[0].getElementsByTagName("billing-address")[0]
    customs = bill.getElementsByTagName("custom-attribute")
    for node in customs:
        info[str(node.attributes.item(0).value)] = getNodeText(node)
    for node in bill.childNodes:
        info[str(node.nodeName)] = getNodeText(node)
    return info


def getLines(node):
    '''lines data'''
    arr = []
    lines = node.getElementsByTagName("product-lineitem")
    for line in lines:
        info1 = {}
        customs = line.getElementsByTagName("custom-attribute")
        adjust = line.getElementsByTagName("price-adjustment")
        tmpAdjust = {}
        for ad in adjust:
            for node in ad.childNodes: 
                tmpAdjust[str(node.nodeName)] = getNodeText(node)
        for node in line.childNodes:
            if(node.nodeName != 'custom-attributes'):
                info1[str(node.nodeName)] = getNodeText(node)
        for node in customs:
            info1[str(node.attributes.item(0).value)] = getNodeText(node)
        info1['price-adjustment'] = tmpAdjust
        arr.append(info1)
    return arr



def getCustomer(node):
    '''order and custmer data'''
    info = {}
    if(len(node.getElementsByTagName("customer-no")) > 0):
        info["customer-no"] = getNodeText(node.getElementsByTagName("customer-no")[0])
    else:
        info["customer-no"] = "Y034001000000001"
    info["currency"] = getNodeText(node.getElementsByTagName("currency")[0])
    info["customer-locale"] = getNodeText(node.getElementsByTagName("customer-locale")[0])
    info["order-no"] = node.getAttribute("order-no")
    info["order-date"] = getNodeText(node.getElementsByTagName("order-date")[0])[0:10]
    customs = node.getElementsByTagName("custom-attribute")
    for node in customs:
        info[str(node.attributes.item(0).value)] = getNodeText(node)
    return info


def getPayment(node):
    '''payment data'''
    info = {}
    payment = node.getElementsByTagName("payment")[0]
    customs = payment.getElementsByTagName("custom-attribute")
    for node in payment.childNodes:
        if(node.nodeName != 'custom-attributes'):
            info[str(node.nodeName)] = getNodeText(node)
    for node in customs:
        info[str(node.attributes.item(0).value)] = getNodeText(node)
    return info




################################ MAPPING 


class OrderDict(dict):

    def __init__(self, file_path):
        doc = minidom.parse(file_path)
        name = doc.getElementsByTagName("order")[0]
        print "Order list : "+str(name.firstChild.data)

        module_dir = os.path.dirname(__file__)  # get current directory
        file_path = os.path.join(module_dir, 'country.json')
        self.country = json.load(open(file_path), encoding='latin1')

        xml_orders = doc.getElementsByTagName("order")
        self.size = len(xml_orders)
        self.orders = []
        self.ids = []
        self.soaps = []
        for order in xml_orders:
            info = {}
            info['shipping'] = getShippingAddress(order)
            info['billing']  = getBillingAddress(order)
            info['lines']    = getLines(order)
            info['customer'] = getCustomer(order)
            info['payment']  = getPayment(order)
            self.orders.append(info)
            self.ids.append(info['customer']['order-no'])
        
        #print self.orders
        #print 'JSON \n \n '
        self.json = json.dumps(self.orders)
        #print self.json


    def createLine(self, product_lineitem):
        ''' Create Lines : Loop on lines '''
        cegid = ''
        cegid += '<ns:Create_Line>\n'
        cegid += '<ns:ItemIdentifier>\n'
        cegid += '<ns:Reference>'+product_lineitem['product-id']+'</ns:Reference>\n'
        cegid += '</ns:ItemIdentifier>\n'


        # Discount
        if(product_lineitem['price-adjustment'] != {}):
            adjust = product_lineitem['price-adjustment'];
            cegid += '<ns:DiscountTypeId>'+adjust['promotion-id'][:4]+'</ns:DiscountTypeId>\n'
            cegid += '<ns:Label>'+escape(product_lineitem['lineitem-text'])+'</ns:Label>\n'
            cegid += '<ns:NetUnitPrice>'+str(float(product_lineitem['net-price']) + float(adjust['net-price']))+'</ns:NetUnitPrice>\n'
            cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
            cegid += '<ns:Quantity>1.00</ns:Quantity>\n'
            cegid += '<ns:Taxes>\n'
            cegid += '<ns:Create_Tax>\n'
            cegid += '<ns:Amount>'+ str(float(product_lineitem['tax']) + float(adjust['tax'])) +'</ns:Amount>\n'
            cegid += '<ns:FamilyId>NOR</ns:FamilyId>'
            cegid += '</ns:Create_Tax>'
            cegid += '</ns:Taxes>\n'
            cegid += '<ns:UnitPrice>'+str(float(product_lineitem['base-price']))+'</ns:UnitPrice>\n'
            cegid += '</ns:Create_Line>\n'
        else:
            cegid += '<ns:Label>'+escape(product_lineitem['lineitem-text'])+'</ns:Label>\n'
            cegid += '<ns:NetUnitPrice>'+product_lineitem['net-price']+'</ns:NetUnitPrice>\n'
            cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
            cegid += '<ns:Quantity>'+product_lineitem['quantity']+'</ns:Quantity>\n'
            cegid += '<ns:Taxes>\n'
            cegid += '<ns:Create_Tax>\n'
            cegid += '<ns:Amount>'+product_lineitem['tax']+'</ns:Amount>\n'
            cegid += '<ns:FamilyId>NOR</ns:FamilyId>'
            cegid += '</ns:Create_Tax>'
            cegid += '</ns:Taxes>\n'
            cegid += '<ns:UnitPrice>'+product_lineitem['base-price']+'</ns:UnitPrice>\n'
            cegid += '</ns:Create_Line>\n'
        return cegid


    def isBackOrder(self, product_lineitems):
        ''' Check if the order is backorder '''
        for line in product_lineitems:
            if(line['LW_preOrderBackOrderHandling'] == 'backorder'):
                return True
        return False


    def cegidXML(self, order):
        ''' Create the SOAP Request XML '''
        cegid  = '\n<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cegid.fr/Retail/1.0">\n'
        cegid += '<soapenv:Header/>\n'
        cegid += '<soapenv:Body>\n'
        cegid +=  '<ns:Create>\n'
        cegid += '<ns:createRequest>\n'

        ###### Delivery Address
        cegid += '<ns:DeliveryAddress>'
        cegid += '<ns:City>'+escape(order['shipping']['city'])+'</ns:City>\n'

        c1 = order['shipping']['country-code'].upper();

        cegid += '<ns:CountryId>'+self.country[c1]['ISO3']+'</ns:CountryId>\n'
        cegid += '<ns:FirstName>'+escape(order['shipping']['last-name'])+'</ns:FirstName>\n'
        cegid += '<ns:LastName>'+escape(order['shipping']['first-name'])+'</ns:LastName>\n'
        cegid += '<ns:Line1>'+escape(order['shipping']['address1'])+'</ns:Line1>\n'

        if('address2' in order['shipping'].keys() ):
            cegid += '<ns:Line2>'+escape(order['shipping']['address2'])+'</ns:Line2>\n'
        cegid += '<ns:PhoneNumber>'+order['shipping']['phone']+'</ns:PhoneNumber>\n'
        if( 'postal-code' in order['shipping'].keys()):
            cegid += '<ns:ZipCode>'+order['shipping']['postal-code']+'</ns:ZipCode>\n'
        cegid += '</ns:DeliveryAddress>'


        ###### Header
        cegid += '<ns:Header>\n'
        cegid += '<ns:Active>true</ns:Active>\n'
        cegid += '<ns:CurrencyId>'+order['customer']['currency']+'</ns:CurrencyId>\n'

        if( 'customer-no' in order['customer'].keys()):
            cegid += '<ns:CustomerId>'+order['customer']['customer-no']+'</ns:CustomerId>\n'
        else:
            cegid += '<ns:CustomerId>Y034001000000001</ns:CustomerId>\n'

        cegid += '<ns:Date>'+order['customer']['order-date'][0:10]+'</ns:Date>\n'
        cegid += '<ns:InternalReference>'+order['customer']['order-no']+'</ns:InternalReference>\n'
        cegid += '<ns:LinesUnmodifiable>false</ns:LinesUnmodifiable>\n'

        ###### OmniChannel
        cegid += '<ns:OmniChannel>\n'
        cegid += '<ns:BillingStatus>Totally</ns:BillingStatus>\n'
        cegid += '<!-- SITE -->\n'
        cegid += '<ns:Comment>'+self.site+'</ns:Comment>\n'
        cegid += '<ns:DeliveryType>ShipByCentral</ns:DeliveryType>\n'
        cegid += '<ns:FollowUpStatus>Validated</ns:FollowUpStatus>\n'



        ###### Gift : TODO
        #print order['shipping']
        if('gift' in order['shipping'].keys()):
            if(order['shipping']['gift'] == 'true'):
                if('gift-message' in order['shipping'].keys()):
                    #print order['shipping']['gift-message']
                    cegid += '<ns:GiftMessage>'+escape(order['shipping']['gift-message'])+'</ns:GiftMessage>\n'
                    cegid += '<ns:GiftMessageType>CustomCard</ns:GiftMessageType>\n'

        #print cegid
        cegid += '<ns:PaymentStatus>Totally</ns:PaymentStatus>\n'
        cegid += '<ns:ReturnStatus>NotReturned</ns:ReturnStatus>\n'
        cegid += '<ns:ShippingStatus>Pending</ns:ShippingStatus>\n'
        cegid += '</ns:OmniChannel>\n'

        ###### Header Part II
        cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
        cegid += '<ns:StoreId>002100</ns:StoreId>\n' # STORE
        cegid += '<ns:TaxExcluded>false</ns:TaxExcluded>\n'

        cegid += '<ns:Type>CustomerOrder</ns:Type>\n'
        cegid += '<ns:UserDefinedTables>\n'



        ###### WAREHOUSE
        if('LW_orderWarehouseID' in order['customer'].keys()):
            cegid += '<!-- DONT FORGET TO CHECK -->\n'
            cegid += '<ns:UserDefinedTable>\n'
            cegid += '<ns:Id>1</ns:Id>\n'
            cegid += '<ns:Value>'+order['customer']['LW_orderWarehouseID']+'</ns:Value>\n'
            cegid += '</ns:UserDefinedTable>\n'
        else:
            cegid += '<!-- DONT FORGET TO CHECK -->\n'
            cegid += '<ns:UserDefinedTable>\n'
            cegid += '<ns:Id>1</ns:Id>\n'
            cegid += '<ns:Value>005ES_WH</ns:Value>\n'
            cegid += '</ns:UserDefinedTable>\n'


        ###### BACKORDER
        if(self.isBackOrder(order['lines'])):
            cegid += '<!-- DONT FORGET TO CHECK -->\n'
            cegid += '<ns:UserDefinedTable>\n'
            cegid += '<ns:Id>2</ns:Id>\n'
            cegid += '<ns:Value>006BACKORDER</ns:Value>\n'
            cegid += '</ns:UserDefinedTable>\n'

        cegid += '</ns:UserDefinedTables>\n'
        cegid += '</ns:Header>\n'


        # Invoicing Address
        cegid += '<ns:InvoicingAddress>\n'
        cegid += '<ns:City>'+escape(order['billing']['city'])+'</ns:City>\n'

        c1 = order['billing']['country-code'].upper();
        cegid += '<ns:CountryId>'+self.country[c1]['ISO3']+'</ns:CountryId>\n'

        cegid += '<ns:FirstName>'+escape(order['billing']['last-name'])+'</ns:FirstName>\n'
        cegid += '<ns:LastName>'+escape(order['billing']['first-name'])+'</ns:LastName>\n'
        cegid += '<ns:Line1>'+escape(order['billing']['address1'])+'</ns:Line1>\n'

        if('address2' in order['billing'].keys() ):
            cegid += '<ns:Line2>'+escape(order['billing']['address2'])+'</ns:Line2>\n'

        cegid += '<ns:PhoneNumber>'+order['billing']['phone']+'</ns:PhoneNumber>\n'
        cegid += '<ns:ZipCode>'+order['billing']['postal-code']+'</ns:ZipCode>\n'
        cegid += '</ns:InvoicingAddress>'

        #Lines
        # Shipping Method
        cegid += '<ns:Lines>\n'
        cegid += '<ns:Create_Line>\n'
        cegid += '<ns:ItemIdentifier>\n'
        cegid += '<ns:Reference>3000000000076</ns:Reference>\n'
        cegid += '</ns:ItemIdentifier>\n'
        cegid += '<ns:Label>'+escape(order['shipping']['item-id'])+'</ns:Label>\n'
        cegid += '<ns:NetUnitPrice>'+order['shipping']['net-price']+'</ns:NetUnitPrice>\n'
        cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
        cegid += '<ns:Quantity>1.00</ns:Quantity>\n'
        cegid += '<ns:Taxes>\n'
        cegid += '<ns:Create_Tax>\n'
        cegid += '<ns:Amount>'+order['shipping']['tax']+'</ns:Amount>\n'
        cegid += '<ns:FamilyId>NOR</ns:FamilyId>\n'
        cegid += '</ns:Create_Tax>\n'
        cegid += '</ns:Taxes>\n'
        cegid += '<ns:UnitPrice>'+order['shipping']['base-price']+'</ns:UnitPrice>\n'
        cegid += '</ns:Create_Line>\n'


        #lines
        for line in order['lines']:
            cegid += self.createLine(line)


        cegid += '</ns:Lines>\n'

        #Payment
        cegid += '<ns:Payments>\n'
        cegid += '<ns:Create_Payment>\n'
        cegid += '<ns:Amount>'+order['payment']['amount']+'</ns:Amount>\n'
        cegid += '<ns:CreditCard>\n'
        cegid += '<ns:TransactionNumber>'+order['payment']['transaction-id']+'</ns:TransactionNumber>\n'
        cegid += '</ns:CreditCard>\n'
        cegid += '<ns:CurrencyId>'+order['customer']['currency']+'</ns:CurrencyId>\n'
        cegid += '<ns:DueDate>'+order['customer']['order-date'][0:10]+'</ns:DueDate>\n'
        cegid += '<ns:Id>1</ns:Id>'
        cegid += '<ns:IsReceivedPayment>false</ns:IsReceivedPayment>\n'

        #TODO : CS EURO 'CYE'

        if(order['payment']['processor-id'] == 'CYBERSOURCE_CREDIT'):
            #cegid += '<ns:MethodId>CYE</ns:MethodId>\n'
            if(order['customer']['currency'] == 'EUR'):
                cegid += '<ns:MethodId>CYE</ns:MethodId>\n'
            elif(order['customer']['currency'] == 'USD'):
                cegid += '<ns:MethodId>CYU</ns:MethodId>\n'
            elif(order['customer']['currency'] == 'GBP'):
                cegid += '<ns:MethodId>CYL</ns:MethodId>\n'
        elif(order['payment']['processor-id'] == 'SA_REDIRECT_PAYPAL'):
            if(order['currency'] == 'EUR'):
                cegid += '<ns:MethodId>PAE</ns:MethodId>\n'
            elif(order['currency'] == 'USD'):
                cegid += '<ns:MethodId>PAU</ns:MethodId>\n'
            elif(order['currency'] == 'GBP'):
                cegid += '<ns:MethodId>PAL</ns:MethodId>\n'
        elif( order['payment']['processor-id'] == 'ADYEN_CREDIT'):
            if(order['customer']['currency'] == 'EUR'):
                cegid += '<ns:MethodId>ADE</ns:MethodId>\n'
            elif(order['customer']['currency'] == 'USD'):
                cegid += '<ns:MethodId>ADU</ns:MethodId>\n'
            elif(order['customer']['currency'] == 'GBP'):
                cegid += '<ns:MethodId>ADL</ns:MethodId>\n'

        cegid += '</ns:Create_Payment>\n'
        cegid += '</ns:Payments>\n'
        cegid += '</ns:createRequest>\n'

         # End
        cegid += '<ns:clientContext>\n'
        cegid += '<!-- DONT FORGET TO CHECK -->\n'
        cegid += '<ns:DatabaseId>LOEWE_DEV2</ns:DatabaseId>\n'
        cegid += '</ns:clientContext>\n'
        cegid += '</ns:Create>\n'
        cegid += '</soapenv:Body>\n</soapenv:Envelope>\n'

        return cegid


    def createSoap(self):
        for order in self.orders:
            self.soaps.append(self.cegidXML(order))



################################ CONVERSION #########################################

def conv(argv):
    order = OrderDict(argv[1])
    order.site = argv[2]
    order.createSoap()
    return [order.ids, order.soaps]

#print conv()