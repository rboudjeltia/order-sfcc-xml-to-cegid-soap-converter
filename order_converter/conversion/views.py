#-*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render
from datetime import datetime

import requests
import json

from .forms import ContactForm
from .forms import NewContactForm
from .forms import OrderForm

from conversion.models import Order
import base64

# /order/create
def create(request):
    sauvegarde = False
    form = OrderForm(request.POST or None, request.FILES)
    print form

    if "file" in form.cleaned_data.keys():
        order = Order()
        order.name = 'test'+str(datetime.now())
        order.file = form.cleaned_data["file"]
        order.save()
        sauvegarde = True
        id = order.id

        return HttpResponseRedirect('/order/show/'+str(id), {'data': 'toto' })
 
    return render(request, 'order_cegid/create.html', {
        'form': form, 
        'sauvegarde': sauvegarde
    })



# /order/show
def show(request, id):
    order = Order.objects.get(id=id)
    order.conv()
    return render(request, 'order_cegid/show.html', {'date': datetime.now(), 'length': len(order.orders), 'data': order, 'message': request.GET})

# /history
def history(request):
    return render(request, 'order_cegid/history.html', {'date': datetime.now(), 'id' : id, 'data': Order.objects.all()})


# POST : /send
def sendOrder(request):
    print request
    #form = OrderForm(request.POST or None, request.FILES)
    data = (request.POST['soap']).encode('utf-8')
    username = request.POST['username']
    password = request.POST['password']
    print username
    print password

    print data
    headers = {
    'Content-Type': 'text/xml;charset=UTF-8',
    'SOAPAction': "http://www.cegid.fr/Retail/1.0/ISaleDocumentService/Create",
    'Authorization': 'Basic '+base64.b64encode(username+':'+password.encode('ascii'))
    }

    url = 'http://nty2web.es.loewe/Y2_DEV/SaleDocumentService.svc'
    response = requests.post(url,  data=data, headers=headers)
    print response.status_code
    print response.text

    return HttpResponse(response.text, content_type='text/xml')



