from xml.etree import cElementTree as ElementTree
import lxml.etree as etree
from pprint import pprint
import json
import sys
from xml.sax.saxutils import escape

from xml.dom.minidom import Text, Element


class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                # treat like dict
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                # treat like list
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)



class XmlDictConfig(dict):
    '''
    Example usage:

    >>> tree = ElementTree.parse('your_file.xml')
    >>> root = tree.getroot()
    >>> xmldict = XmlDictConfig(root)

    Or, if you want to use an XML string:

    >>> root = ElementTree.XML(xml_string)
    >>> xmldict = XmlDictConfig(root)

    And then use xmldict for what it is... a dict.
    '''
    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                # treat like dict - we assume that if the first two tags
                # in a series are different, then they are all different.
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                # treat like list - we assume that if the first two tags
                # in a series are the same, then the rest are the same.
                else:
                    # here, we put the list in dictionary; the key is the
                    # tag name the list elements all share in common, and
                    # the value is the list itself 
                    aDict = {element[0].tag: XmlListConfig(element)}
                # if the tag has attributes, add those to the dict
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            # this assumes that if you've got an attribute in a tag,
            # you won't be having any text. This may or may not be a 
            # good idea -- time will tell. It works for the way we are
            # currently doing XML configuration files...
            elif element.items():
                self.update({element.tag: dict(element.items())})
            # finally, if there are no child tags and no attributes, extract
            # the text
            else:
                self.update({element.tag: element.text})
       

    def createLine(self, product_lineitem):
        ''' Create Lines : Loop on lines '''
    
        cegid = ''
        cegid += '<ns:Create_Line>\n'
        cegid += '<ns:ItemIdentifier>\n'
        cegid += '<ns:Reference>'+product_lineitem['product-id']+'</ns:Reference>\n'
        cegid += '</ns:ItemIdentifier>\n'

        # Discount 
        if('price-adjustment' in product_lineitem['price-adjustments'].keys()):
            adjust = product_lineitem['price-adjustments']['price-adjustment'];
            cegid += '<ns:DiscountTypeId>'+adjust['promotion-id'][:4]+'</ns:DiscountTypeId>\n'
            cegid += '<ns:Label>'+escape(product_lineitem['lineitem-text'])+'</ns:Label>\n'
            cegid += '<ns:NetUnitPrice>'+str(float(product_lineitem['gross-price']) + float(adjust['gross-price']))+'</ns:NetUnitPrice>\n'
            cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
            cegid += '<ns:Quantity>1.00</ns:Quantity>\n'
            cegid += '<ns:Taxes>\n'
            cegid += '<ns:Create_Tax>\n'
            cegid += '<ns:Amount>'+ str(float(product_lineitem['tax']) + float(adjust['tax'])) +'</ns:Amount>\n'
            cegid += '<ns:FamilyId>NOR</ns:FamilyId>'
            cegid += '</ns:Create_Tax>'
            cegid += '</ns:Taxes>\n'
            cegid += '<ns:UnitPrice>'+str(float(product_lineitem['base-price']))+'</ns:UnitPrice>\n'
            cegid += '</ns:Create_Line>\n'        
        

        else:
            # If discount
            #if(float(product_lineitem['gross-price']) != float(product_lineitem['base-price']) ):
            #cegid += '<ns:DiscountTypeId>CORM</ns:DiscountTypeId>\n'

            cegid += '<ns:Label>'+escape(product_lineitem['lineitem-text'])+'</ns:Label>\n'
            cegid += '<ns:NetUnitPrice>'+product_lineitem['gross-price']+'</ns:NetUnitPrice>\n'
            cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
            cegid += '<ns:Quantity>1.00</ns:Quantity>\n'
            cegid += '<ns:Taxes>\n'
            cegid += '<ns:Create_Tax>\n'
            cegid += '<ns:Amount>'+product_lineitem['tax']+'</ns:Amount>\n'
            cegid += '<ns:FamilyId>NOR</ns:FamilyId>'
            cegid += '</ns:Create_Tax>'
            cegid += '</ns:Taxes>\n'
            cegid += '<ns:UnitPrice>'+product_lineitem['base-price']+'</ns:UnitPrice>\n'
            cegid += '</ns:Create_Line>\n'        
        
        return cegid


    def isBackOrder(self, product_lineitems):
        ''' Check if the order is backorder '''

        if (not isinstance(product_lineitems, list)):
            line = product_lineitems
            if('custom-attributes' in line.keys()):
                if('backorder' in line['custom-attributes']['custom-attribute']):
                    return True
                else:
                    return False
        else:
            for i in range(0, len(product_lineitems)):
                line = product_lineitems[i]
                if('custom-attributes' in line.keys()):
                    if('backorder' in line['custom-attributes']['custom-attribute']):
                        return True
        return False    



    def cegidXML(self, orders, site, i):
        ''' Create the SOAP Request XML '''

        order = orders[i]

        country = json.load(open('country.json'), encoding='latin1')

        cegid  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cegid.fr/Retail/1.0">\n'
        cegid += '<soapenv:Header/>\n'
        cegid += '<soapenv:Body>\n'
        cegid +=  '<ns:Create>\n'
        cegid += '<ns:createRequest>\n'

        # Delivery Address
        cegid += '<ns:DeliveryAddress>'
        cegid += '<ns:City>'+escape(order['shipments']['shipment']['shipping-address']['city'])+'</ns:City>\n'

        c1 = order['shipments']['shipment']['shipping-address']['country-code'].upper();
        cegid += '<ns:CountryId>'+country[c1]['ISO3']+'</ns:CountryId>\n'
        cegid += '<ns:FirstName>'+escape(order['shipments']['shipment']['shipping-address']['last-name'])+'</ns:FirstName>\n'
        cegid += '<ns:LastName>'+escape(order['shipments']['shipment']['shipping-address']['first-name'])+'</ns:LastName\n>'
        cegid += '<ns:Line1>'+escape(order['shipments']['shipment']['shipping-address']['address1'])+'</ns:Line1>\n'
        
        if('address2' in order['shipments']['shipment']['shipping-address'].keys() ):
            cegid += '<ns:Line2>'+escape(order['shipments']['shipment']['shipping-address']['address2'])+'</ns:Line2>\n'

        cegid += '<ns:PhoneNumber>'+order['shipments']['shipment']['shipping-address']['phone']+'</ns:PhoneNumber>\n'
        if( 'postal-code' in order['shipments']['shipment']['shipping-address'].keys()):
            cegid += '<ns:ZipCode>'+order['shipments']['shipment']['shipping-address']['postal-code']+'</ns:ZipCode>\n'
        cegid += '</ns:DeliveryAddress>'

        # Header
        cegid += '<ns:Header>\n'
        cegid += '<ns:Active>true</ns:Active>\n'
        cegid += '<ns:CurrencyId>'+order['currency']+'</ns:CurrencyId>\n'

        if( 'customer-no' in order['customer'].keys()):
            cegid += '<ns:CustomerId>'+order['customer']['customer-no']+'</ns:CustomerId>\n'
        else:
            cegid += '<ns:CustomerId>Y034001000000001</ns:CustomerId>\n'

        cegid += '<ns:Date>'+order['order-date'][0:10]+'</ns:Date>\n'
        cegid += '<ns:InternalReference>'+order['current-order-no']+'</ns:InternalReference>\n'
        cegid += '<ns:LinesUnmodifiable>false</ns:LinesUnmodifiable>\n'
       
        # OmniChannel
        cegid += '<ns:OmniChannel>\n'
        cegid += '<ns:BillingStatus>Totally</ns:BillingStatus>\n'
        cegid += '<!-- SITE -->\n'
        cegid += '<ns:Comment>'+site+'</ns:Comment>\n' 
        cegid += '<ns:DeliveryType>ShipByCentral</ns:DeliveryType>\n'
        cegid += '<ns:FollowUpStatus>Validated</ns:FollowUpStatus>\n'

        # Gift : TODO
        if(order['shipments']['shipment']['gift'] == 'true'):
            if('gift-message' in order['shipments']['shipment'].keys()):
                cegid += '<ns:GiftMessage>'+escape(order['shipments']['shipment']['gift-message'])+'</ns:GiftMessage>\n'
                cegid += '<ns:GiftMessageType>CustomCard</ns:GiftMessageType>\n'
            else:
                cegid += '<ns:GiftMessage></ns:GiftMessage>\n'
                cegid += '<ns:GiftMessageType>CustomCard</ns:GiftMessageType>\n'


        cegid += '<ns:PaymentStatus>Totally</ns:PaymentStatus>\n'
        cegid += '<ns:ReturnStatus>NotReturned</ns:ReturnStatus>\n'
        cegid += '<ns:ShippingStatus>Pending</ns:ShippingStatus>\n'
        cegid += '</ns:OmniChannel>\n'

        # Header Part II
        cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
        cegid += '<ns:StoreId>002100</ns:StoreId>\n' # STORE
        cegid += '<ns:TaxExcluded>false</ns:TaxExcluded>\n'

        cegid += '<ns:Type>CustomerOrder</ns:Type>\n'
        cegid += '<ns:UserDefinedTables>\n'
        


        # WAREHOUSE
        #return order['custom-attributes']
        if(len(order['custom-attributes']['custom-attribute']) == 2):
            #return order['custom-attributes']['custom-attribute'][1]
            cegid += '<!-- DONT FORGET TO CHECK -->\n'
            cegid += '<ns:UserDefinedTable>\n'
            cegid += '<ns:Id>1</ns:Id>\n'
            cegid += '<ns:Value>'+order['custom-attributes']['custom-attribute'][1]+'</ns:Value>\n'
            cegid += '</ns:UserDefinedTable>\n'
        else:
            cegid += '<!-- DONT FORGET TO CHECK -->\n'
            cegid += '<ns:UserDefinedTable>\n'
            cegid += '<ns:Id>1</ns:Id>\n'
            cegid += '<ns:Value>005ES_WH</ns:Value>\n'
            cegid += '</ns:UserDefinedTable>\n'



        # BACKORDER
        

        if(self.isBackOrder(order['product-lineitems']['product-lineitem'])):
            cegid += '<!-- DONT FORGET TO CHECK -->\n'
            cegid += '<ns:UserDefinedTable>\n'
            cegid += '<ns:Id>2</ns:Id>\n'
            cegid += '<ns:Value>006BACKORDER</ns:Value>\n' # BackOrder
            cegid += '</ns:UserDefinedTable>\n'
        
        cegid += '</ns:UserDefinedTables>\n'
        cegid += '</ns:Header>\n'


        # Invoicing Address
        cegid += '<ns:InvoicingAddress>\n'
        cegid += '<ns:City>'+escape(order['customer']['billing-address']['city'])+'</ns:City>\n'
        
        c1 = order['customer']['billing-address']['country-code'].upper();
        cegid += '<ns:CountryId>'+country[c1]['ISO3']+'</ns:CountryId>\n'

        cegid += '<ns:FirstName>'+escape(order['customer']['billing-address']['last-name'])+'</ns:FirstName>\n'
        cegid += '<ns:LastName>'+escape(order['customer']['billing-address']['first-name'])+'</ns:LastName>\n'
        cegid += '<ns:Line1>'+escape(order['customer']['billing-address']['address1'])+'</ns:Line1>\n'

        if('address2' in order['customer']['billing-address'].keys() ):
            cegid += '<ns:Line2>'+escape(order['customer']['billing-address']['address2'])+'</ns:Line2>\n'

        cegid += '<ns:PhoneNumber>'+order['customer']['billing-address']['phone']+'</ns:PhoneNumber>\n'
        cegid += '<ns:ZipCode>'+order['customer']['billing-address']['postal-code']+'</ns:ZipCode>\n'
        cegid += '</ns:InvoicingAddress>'
        

        #Lines
        # Shipping Method
        cegid += '<ns:Lines>\n'
        cegid += '<ns:Create_Line>\n'
        cegid += '<ns:ItemIdentifier>\n'
        cegid += '<ns:Reference>3000000000076</ns:Reference>\n'
        cegid += '</ns:ItemIdentifier>\n'
        cegid += '<ns:Label>'+escape(order['shipments']['shipment']['shipping-method'])+'</ns:Label>\n'
        cegid += '<ns:NetUnitPrice>'+order['shipments']['shipment']['totals']['shipping-total']['net-price']+'</ns:NetUnitPrice>\n'
        cegid += '<ns:Origin>ECommerce</ns:Origin>\n'
        cegid += '<ns:Quantity>1.00</ns:Quantity>\n'
        cegid += '<ns:Taxes>\n'
        cegid += '<ns:Create_Tax>\n'
        cegid += '<ns:Amount>'+order['shipments']['shipment']['totals']['shipping-total']['tax']+'</ns:Amount>\n'
        cegid += '<ns:FamilyId>NOR</ns:FamilyId>\n'
        cegid += '</ns:Create_Tax>\n'
        cegid += '</ns:Taxes>\n'
        cegid += '<ns:UnitPrice>'+order['shipments']['shipment']['totals']['shipping-total']['gross-price']+'</ns:UnitPrice>\n'
        cegid += '</ns:Create_Line>\n'


        if (isinstance(order['product-lineitems']['product-lineitem'], list)):
            for i in range(0, len(order['product-lineitems']['product-lineitem'])):
                cegid += self.createLine(order['product-lineitems']['product-lineitem'][i])
        else:
            cegid += self.createLine(order['product-lineitems']['product-lineitem'])

        cegid += '</ns:Lines>\n'

        #Payment
        cegid += '<ns:Payments>\n'
        cegid += '<ns:Create_Payment>\n'
        cegid += '<ns:Amount>'+order['payments']['payment']['amount']+'</ns:Amount>\n'
        cegid += '<ns:CreditCard>\n'
        cegid += '<ns:TransactionNumber>'+order['payments']['payment']['transaction-id']+'</ns:TransactionNumber>\n'
        cegid += '</ns:CreditCard>\n'
        cegid += '<ns:CurrencyId>'+order['currency']+'</ns:CurrencyId>\n'
        cegid += '<ns:DueDate>'+order['order-date'][0:10]+'</ns:DueDate>\n'
        cegid += '<ns:Id>1</ns:Id>'
        cegid += '<ns:IsReceivedPayment>false</ns:IsReceivedPayment>\n'


        #TODO : CS EURO 'CYE'


        if(order['payments']['payment']['processor-id'] == 'CYBERSOURCE_CREDIT'):
            cegid += '<ns:MethodId>CYE</ns:MethodId>\n'
            #if(order['currency'] == 'EUR'):
                #cegid += '<ns:MethodId>CYE</ns:MethodId>\n'
            #elif(order['currency'] == 'USD'):
                #cegid += '<ns:MethodId>CYU</ns:MethodId>\n'
            #elif(order['currency'] == 'GBP'):
                #cegid += '<ns:MethodId>CYL</ns:MethodId>\n'
        elif(order['payments']['payment']['processor-id'] == 'SA_REDIRECT_PAYPAL'):
            if(order['currency'] == 'EUR'):
                cegid += '<ns:MethodId>PAE</ns:MethodId>\n'
            elif(order['currency'] == 'USD'):
                cegid += '<ns:MethodId>PAU</ns:MethodId>\n'
            elif(order['currency'] == 'GBP'):
                cegid += '<ns:MethodId>PAL</ns:MethodId>\n'
        elif( order['payments']['payment']['processor-id'] == 'ADYEN_CREDIT'):
            if(order['currency'] == 'EUR'):
                cegid += '<ns:MethodId>ADE</ns:MethodId>\n'
            elif(order['currency'] == 'USD'):
                cegid += '<ns:MethodId>ADU</ns:MethodId>\n'
            elif(order['currency'] == 'GBP'):
                cegid += '<ns:MethodId>ADL</ns:MethodId>\n'

        cegid += '</ns:Create_Payment>\n'
        cegid += '</ns:Payments>\n'
        cegid += '</ns:createRequest>\n'

         # End
        cegid += '<ns:clientContext>\n'
        cegid += '<!-- DONT FORGET TO CHECK -->\n'
        cegid += '<ns:DatabaseId>LOEWE_TEST</ns:DatabaseId>\n'
        cegid += '</ns:clientContext>\n'
        cegid += '</ns:Create>\n'
        cegid += '</soapenv:Body>\n</soapenv:Envelope>\n'
        
        return cegid
        





def main(argv):
    it = ElementTree.iterparse(argv[1])
    #pprint(it)
    for _, el in it:
        if '}' in el.tag:
            el.tag = el.tag.split('}', 1)[1]  # strip all namespaces 
    root = it.root
    xmldict = XmlDictConfig(root)

    site = argv[2];

    if(argv[3] == 'json'): 

        print json.dumps(xmldict['orders'])
    elif(argv[3] == 'xml'):
        #pprint(xmldict.cegidXML())
        import collections
        if(not isinstance(xmldict['orders']['order'], list)):
            arr = [xmldict['orders']['order']]
        else:
            arr = xmldict['orders']['order']

        print len(arr)

        for i in range(0, len(arr)) :
            xml = xmldict.cegidXML(arr, site, i).encode('utf-8')
            print xml
    pass




#if __name__ == "__main__":
#    main(sys.argv)



#tree = ElementTree.parse('./PRD-00046425.xml')


#print xmldict
#print json.dumps(xmldict)