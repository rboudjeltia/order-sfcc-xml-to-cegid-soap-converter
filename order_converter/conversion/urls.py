from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/order/create')),
    url(r'^create/$', views.create, name='order'),
    url(r'^history/$', views.history, name='history'),

	url(r'^send/$', views.sendOrder, name='send'),

    url(r'^show/(?P<id>\w{0,50})/$', views.show, name='show'),
]