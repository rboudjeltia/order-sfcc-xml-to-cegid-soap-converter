#-*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from datetime import datetime

# Create your models here.
from xml.etree import cElementTree as ElementTree
import lxml.etree as etree
from pprint import pprint
import json
import sys
from xml.sax.saxutils import escape

from xml.dom.minidom import Text, Element

#from modules.order_conv import conv
from order_conv import conv, OrderDict


class Order(models.Model):
	name   = models.CharField(max_length=255)
	file   = models.FileField(upload_to="orders/")
	date   = models.DateTimeField(default=datetime.now, blank=True)
	count_order  = models.IntegerField(default=0)

	soaps = []

	def conv(self):
		print("Conversion : "+str(self.file))
		arr = conv(['truc', self.file, 'LOE_EUR', 'xml'])
		self.soaps = arr[1]
		self.orders = arr[0]
		self.count_order = len(self.orders)
		self.save()
		print self.orders
		return 0

	    
	def __str__(self):
		return self.name